
INTRODUCTION
------------

The Background Information module is for the existing Drupal "Site Information" form.


REQUIREMENTS
------------

None.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module or follow following steps:

1. Move or copy or extract the 'background_information' folder to sites/all/modules.

2. Enable the module 'Background Information' on the page admin/build/modules.

3. Clear your Drupal cache at admin/settings/performance by clicking
'Clear cached data'.

CONFIGURATION
-------------

1. Go to admin/config/system/site-information and type the value for "site unique key"
in the text field.

2. Save configuration.


MAINTAINERS
-----------

Current maintainer:
* Kumar Rahul Sankrit (rahul_sankrit) - https://www.drupal.org/user/2678667